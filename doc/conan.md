---
layout: page
title: Conan
nav_order: 1
---

* TOC
{:toc}

# Install (ubuntu 20.04)
```bash
sudo apt install python3 python3-pip
pip3 install conan
# Reboot !
```

# Cache & Remote (packages repo)
## Local cache
Present by default in `~/.conan`
```bash
conan search
```

## Local server (usefull for sharing across team)
```bash
conan_server

# use it with
conan remote add local http://0.0.0.0:9300
```
### Config
Edit config in ~/.conan_server/server.conf
 * default user:pass demo:demo
 * add permission for specific/all to write to local

## Community remote
```bash
conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan
```

# Using conan
## For dependencies only
Dependencies are specified in **conanfile.txt** ([Doc](https://docs.conan.io/en/latest/reference/conanfile_txt.html))
```bash
# from the project folder
mkdir && cd build
conan install .. --build=missing
cmake ..
make -j8
```

## conanfile.py
The conan package can then be referenced to by other projects  
First, create a package folder (that may be followed by git)
```bash
mkdir my_pkg && cd my_pkg
conan new my_lib/1.2.3

# optional : follow with git
git init
git add conanfile.py
git commit -am "Add conanfile.py"
```
If **conanfile.py** is directly present in the source folder, create it with
```bash
conan new my_lib/1.2.3 -s
```

**conanfile.py** contains several functions :
 * ConanFile parameters
 ```python
 class MyLibConan(ConanFile)
     name = ""
     version = ""
     license = ""
     author = ""
     url = ""
     description = ""
     topics = ""
     generators = "cmake"
     requires = "boost_chrono/1.65.1@bincrafters/stable", "boost_date_time/1.65.1@bincrafters/stable"
 ```
 * source() to ge the source, typically contains
  ```python
  self.run("git clone path/to/my_lib.git -b my/branch")
  ```
  This function doesn't exist if **conanfile.py** is already in the source folder
 * build() typically contains (assuming cmake is used) :
  ```python
    cmake = CMake(self)
    cmake.configure(source_folder="etta")
    # if conanfile.py is in source folder use cmake.configure()
    cmake.build()
  ```
 * package() copies the files to export
  ```python
    self.copy("*.h", dst="include", src="include")
    self.copy("*.so", dst="lib", src="build/lib", keep_path=False)
  ```
 * package_info()
  ```python
    self.cpp_info.libs = ["my_lib"]
  ```
 
More info at [Methods reference](https://docs.conan.io/en/latest/reference/conanfile/methods.html)

### Test
Test source and build functions with :
```bash
conan install . -if build
conan source . -if buid -sf build
conan build . -bf build
conan package . -bf build --pf package
```

## Create
```bash
# install deps, build and place binaries into local cache
conan create path/to/folder

# upload package to remote
# OPTIONAL, as package are locally stored in conan cache by default
conan upload -r remote_name my_lib*

# boost link to static by default, to link dynamically
conan create . -o boost*:shared=True --build=missing

# link libstdc++ (old ABI) or libstdc++11 (new ABI)
conan create . -s compiler.libcxx=libstdc++
conan create . -s compiler.libcxx=libstdc++11
```

## Virtual run env
Ensure package copies the generated bin
```self.copy("my-app", dst="bin", src="bin", keep_path=False)```
```bash
conan install -if my-app-env my-app@user/channel -g virtualrunenv

source my-app-env/activate_run.sh
source my-app-env/deactivate_run.sh
```

## Removing a package
```bash
conan remove mylib*
```

## Deploy
```bash
conan install package/0.1.2@user/channel -g deploy -if folder
```

# CMake
## All at once (simple)

```bash
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

add_executable(timer timer.cpp)
# add include and link libs
target_link_libraries(timer ${CONAN_LIBS})
```
## Per TARGET
Every requirements get corresponding VARs defined for CMake. Here, the dep is called **dependency**
```bash
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup(TARGETS)

add_executable(timer timer.cpp)
# add include dirs
target_include_directories(timer ${CONAN_INCLUDE_DIRS_DEPENDENCY})
# add link libs only, using full path to deps
target_link_libraries(timer CONAN_PKG::dependency)

```
