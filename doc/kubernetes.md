---
layout: page
title: Kubernetes
nav_order: 1
---

* TOC
{:toc}


# Concepts & Keywords
* **cluster** : host nodes
* **node** : workers on which pods are running  
  All pods in a cluster can reach each others without NAT
  * **kubelets** : implements kubernetes API to node, controlled by master
* **pod** : logical host of an app (containers, VMs, networks, volumes, etc)  
  All containers in a pod can reach each others on localhost
* **deployment** : keep a set of pods running
* **service** : make a deployment available, logical set of pods (replicates, access, etc)
  * **cluster ip** : service only reachable within cluster
  * **NopePort** : uses NAT to expose ports
  * **LoadBalancer** : superset of NodePort, external load balancer
  * **ExternalName** : exposes the service using arbitrary name

# Commands
## minikube cluster
minikube runs a local cluster. This local cluster runs on a VM, thus has a separate docker daemon than the host machine
```bash
minikub start|stop|status|delete|dashboard -p cluster-name
# Sub-sequent kubectl commands are directed to that cluster

# Get default config (including cluster-name)
kubectl config view
# Get default cluster
kubectl config get-contexts
# Set cluster-name as default
kubectl config set-context cluster-name
kubectl config set-cluster cluster-name

# list profiles
minikube profile list
```

### minikube cluster and local docker
minikube runs in a docke (or VM) and has a separate docker daemon
```bash
# use minicube docker
eval $(minikube -p cluster-name docker-env)

# do not use latest tag, as it will force pulling from docker hub
docker build -f path/to/Dockerfile -t mytag:local
```

```yml
# In kubernetes container object declaration
# set imagePullPolicy to Never, it is defaulted to Always, which polls from DockerHub
containers:
  - name: my-container
    image: my-image:local
    imagePullPolicy: Never
```

## kubectl
control and inspect kubernetes

### create/delete
```bash
# file.yaml describes an obect with kind: Node|Pod|Deployment|Service
kubectl create -f file.yaml
kubectl create deployment name --image=docker-img.io/image

kubectl expose deployment name --type=NodePort --port=8080

kubectl delete service,pod,deployment name
```

### status/describe
```bash
# list resources available
kubectl api-resources

# list running nodes, pods, services and deployments
kubectl get|describe nodes|no
kubectl get|describe podes|po
kubectl get|describe services|svc
kubectl get|describe deploy

# list podes based on filters
#   app=my-app depends on depoyment metadata
#   -o wide to get more output raws
kubectl get podes -l app=my-app -o wide

# list service
kubectl get|describe svc my-service

# get endpoints
kubectl get ep my-service
```

### interact
```bash
#any command
kubectl exec pod-name -- <cmd>
kubectl exec -ti pod-name -- bash
kubectl exec -ti pod-name -c container-name -- bash

# log, ie STDOUT
kubectl logs pod-name
kubectl logs pod-name -c container-name

# proxy, to access pod isolated network (TODO clarify)
kubectl proxy
```

#### minikube
```bash
# access cluster with minikube
minikube ssh -p my-cluster

# minikube doesn't expose services as it should after applying a service config
# use port-forward to workaround that limitation
kubectl port-forward service/my-service --address a.b.c.d exposed-port/pod-port
# accept any source address
kubectl port-forward service/my-service --address 0.0.0.0 exposed-port/pod-port
```

