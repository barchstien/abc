---
layout: page
title: Anaconda
nav_order: 1
---

* TOC
{:toc}
# Install (one-time only)
```bash
# use latest link from website
wget https://repo.anaconda.com/archive/Anaconda3-2019.07-Linux-x86_64.sh
chmod u+x Anaconda3-*
./Anaconda3-[...]
```
**Do not install Anaconda in your PATH or .bashrc**

# Usage
```bash
# load conda
source /home/bastien/anaconda3/etc/profile.d/conda.sh
# info on installed conda
conda info

# create an environment from scratch
conda create --name my_env python=3.7
# create an environment from a description file
conda env create -f my_env.yml

# list existing environments
conda env list

# activate env
conda activate my_env

# install package to env
conda install bokeh pandas
# install from conda-forge channel
conda install -c conda-forge geopy verde

# export the env to YML
conda env export > environment.yml

# deactivate env
conda deactivate
```
