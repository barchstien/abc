---
layout: page
title: Python
nav_order: 1
---


```python
python3 -m venv my-env
source my-env/bin/activate

pip3 install --upgrade pip

# use deactivate to exit venv
```

```python
exec(open('myscript.py').read())
```
