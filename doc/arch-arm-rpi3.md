---
layout: page
title: Arch ARM Rpi3
nav_order: 1
---

# TODO
 * read only
 * easier BT pairing <-- python ?
 * make a step by step instead of by theme

# Install
[Arch ARM Rpi install](https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3)

# Pre-boot Config
Add to`/boot/config.txt` :
```bash
# EEPROM dtree load not suppored by kernel >= 5.4
force_eeprom_read=0
dtoverlay=hifiberry-dacplus
# bluetooth
dtparam=krnbt=on
# wifi + bluetooth = problem
dtoverlay=disable-wifi

```

# Post-boot config
```bash
pacman-key --init
pacman-key --populate archlinuxarm

pacman -S htop nano

# full upgrade, takes long time...
pacman -Syu

# Alsa
pacman -Sy alsa-utils alsa-firmware alsa-lib alsa-plugins
usermod -a -G audio alarm
```
## HifiBerry DAC+
Add to `/etc/asound.conf`
```bash
pcm.!default {
  type hw card 0
}
ctl.!default {
  type hw card 0
}
```
Test with `speaker-test -c2`


## Bluetooth
**WARNING :** Bluez 5.63 is reported to be buggy, use instead 5.61, which can be found in Arch ARM Archive pkgs  
**NOTE :** I first wanted to re-build using PKGBUILD, but even when using the snashot from github, it didn't build (bad function signature...)
```bash
# do not install default 5.63, coz bluetooth doesn't connect to anyone...
# pacman -S bluez bluez-utils bluez-tools pipewire-pulse

pacman -U <path-to-5.61-pkgs>

# use wireplumber
pacman -S pipewire-pulse
systemctl --user --now enable wireplumber
```

Add to `/etc/bluetooth/main.conf`
```bash
[General]
# prettyhostname-MAC_like
Name = %h-%d
# Source, ??
Enable=Source,Sink,Media,Control
# Hifi Audio Device
Class = 0x200428
# Always discoverable 
DiscoverableTimeout = 0
AlwaysPairable = true

[Policy]
AutoEnable = true
```

**Note : ** more classes at http://bluetooth-pentest.narod.ru/software/bluetooth_class_of_device-service_generator.html

Add to `/etc/machine-info`
```bash
PRETTY_HOSTNAME=bt-voli
```

```bash
systemctl enable bluetooth
shutdown -r now
```

## Systemd Agent
**Note** : bt-agent rquires `blues-tools`
`nano /etc/systemd/system/bt-voli-agent.service`
```
[Unit]
Description=BT Agent no input no output

[Service]
ExecStart=/bin/sh -c '/usr/bin/bt-agent -c NoInputNoOutput | /usr/bin/yes yes
After=bluetooth.service

[Install]
WantedBy=multi-user.target
```

## bluetoothctl
```bash
scan on

# MAC works with auto-complete, ie TAB
trust MAC
pair MAC
connect MAC

scan off
```
# Sources
[Arch ARM Rpi install](https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3)
[Arch ARM Rpi](https://archlinuxarm.org/wiki/Raspberry_Pi)
[Arch ARM archive pkgs](http://tardis.tiny-vps.com/aarm/)
[Arch ARM PipeWire](https://wiki.archlinux.org/title/PipeWire)
[Hifiberry DAC+ Config](https://www.hifiberry.com/docs/software/configuring-linux-3-18-x/)
[Bluez DBus agent API](https://git.kernel.org/pub/scm/bluetooth/bluez.git/tree/doc/agent-api.txt)

