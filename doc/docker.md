---
layout: page
title: Docker
nav_order: 1
---

* TOC
{:toc}

# Install (ubuntu 20.04)
```bash
sudo apt install docker.io
# add current user to group
sudo groupadd docker
sudo usermod -aG docker ${USER}
# reload current user session
su - ${USER}
```

## Test
```bash
docker run hello-world
```

# Dockerfile
```dockerfile
FROM ubuntu:20.04

# Avoid any human interaction from apt
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y build-essential cmake python3-pip openssh-server git

# assuming host uid:gid is 1000:1000
RUN groupadd -r -g 1000 docker-user && useradd -r -u 1000 -g docker-user -m docker-user
USER 1000:1000
ENV PATH="/home/docker-user/.local/bin:${PATH}"
RUN pip3 install conan
```

## Default command
```dockerfile
# default file to execute
COPY default_script.sh /root/

# default commands
CMD ["/root/default_script.sh"]
```

## Setup conan if .conan is not mounted
```dockerfile
RUN conan profile new default --detect
RUN conan profile update settings.compiler.libcxx=libstdc++11 default
```

## Copy ssh id (don't do that, mount the host ~/.ssh)
```dockerfile
RUN mkdir -p /root/.ssh && \
    chmod 0700 /root/.ssh

# requires using :
# --build-arg ssh_prv_key="$(cat ~/.ssh/id_rsa)" --build-arg ssh_pub_key="$(cat ~/.ssh/id_rsa.pub)"
# when building the image
ARG ssh_prv_key
ARG ssh_pub_key
RUN echo "$ssh_prv_key" > /root/.ssh/id_rsa && \
    echo "$ssh_pub_key" > /root/.ssh/id_rsa.pub && \
    chmod 600 /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/id_rsa.pub && \
    echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config && \
    eval `ssh-agent -s` && \
    ssh-add /root/.ssh/id_rsa

```

# Commands
## Build
Each command in **Dockerfile** is cached during the first execution and is only re-executer if changed. Each command creates a read-only layer, one for each cache level

```bash
cd path/to/Dockerfile
docker build . -t mytag
```

### Build with a copy of ssh id (don't do that, mount the host ~/.ssh)
```bash
# use user ssh id
docker build -t mytag \
  --build-arg ssh_prv_key="$(cat ~/.ssh/id_rsa)" \
  --build-arg ssh_pub_key="$(cat ~/.ssh/id_rsa.pub)" .
```

## Run
Creates a container, ie a writable layer
```bash
# --rm removes the container after exits
# run the default, ie CMD in Dockerfile
docker run --rm mytag

# bash
docker run -it --rm mytag /bin/bash

# mount path
docker run -v$(pwd):/root/mydir -it --rm mytag /bin/bash

# mount ssh, conan and current folder
docker run \
  -u$(id -u):$(id -g) \
  -v$(echo $HOME)/.ssh:/home/docker-user/.ssh:ro \
  -v$(echo $HOME)/.conan:/home/docker-user/.conan \
  -v$(pwd):/home/docker-user/workdir \
  -w /home/docker-user/workdir -it --rm test-build /bin/bash

```

## Exec
same as run but with an exiting container

## Images
```bash
docker images -a
docker image prune
docker image rm ID
```

## Containers
```bash
docker container list -a
docker prune
docker rm ID
```

# Examples
## Foundry Virtual Table Top
````bash
Install
=======
# get foundryvtt-x.y.z.zip
# app folder
unzip foundryvtt-0.8.9.zip -d foundryvtt
# data folder
mkdir data
 
Test
====
# run and delete container on ctrl+c
# mount app and data folders
# publish port 30000
# use node, node.js docker image
# start node with known app and data path
# accept any IP connect on port 30000 of docker host
docker run -it --rm -v$(pwd)/foundryvtt:/mnt/foundryvtt -v$(pwd)/data:/mnt/data --publish 30000:30000 --name fvtt node node /mnt/foundryvtt/resources/app/main.js --dataPath=/mnt/data

Run
===
# detach and live forever (survives reboot)
docker run -d --restart always -v$(pwd)/foundryvtt:/mnt/foundryvtt -v$(pwd)/data:/mnt/data --publish 30000:30000 --name fvtt node node /mnt/foundryvtt/resources/app/main.js --dataPath=/mnt/data

# also works with node:alpine for smaller footprint
````
